package com.epam.esm.controller;

import com.epam.esm.dto.PagedResult;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.services.GiftCertificateServiceV2;
import com.epam.esm.services.TagServiceV2;
import com.epam.esm.specs.TagSpecs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Tag controller for connecting with client
 *
 * @author Abduvohid Isroilov
 */
@RestController
public class TagController {

    private final TagServiceV2 tagService;
    private final GiftCertificateServiceV2 giftCertificateService;

    public TagController(TagServiceV2 tagService, GiftCertificateServiceV2 giftCertificateService) {
        this.tagService = tagService;
        this.giftCertificateService = giftCertificateService;
    }

    /**
     * Getting all tags
     *
     * @return ResponseEntity<?>
     */
    @GetMapping("/tag")
    public ResponseEntity<?> get(
            @RequestParam(value = "name", required = false) Set<String> names, Pageable pageable
    ) {

        TagSpecs tagSpecs = new TagSpecs();
        tagSpecs.setNames(names);

        Page<TagDto> dtos = tagService.getAllByPagination(tagSpecs, pageable);

        setHateoasLinks(dtos);

        PagedResult pagedResult = getPagedResult(dtos);

        return new ResponseEntity<>(pagedResult, HttpStatus.OK);

    }

    private void setHateoasLinks(Page<TagDto> dtos) {
        dtos.getContent()
                .forEach(dto -> {
                    Link selfLink = linkTo(TagController.class).slash("/tag").withSelfRel();
                    dto.add(selfLink);
                    Link dtoLink = linkTo(methodOn(TagController.class)
                            .getById(dto.getId())).withRel("tag");
                    dto.add(dtoLink);
                });
    }

    private PagedResult getPagedResult(Page<TagDto> dtos) {
        PagedResult pagedResult = new PagedResult();
        pagedResult.buildMetadataFrom(dtos);
        pagedResult.setItems(dtos.getContent());
        return pagedResult;
    }

    /**
     * Getting Tag by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/tag/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {

        TagDto tagDto = tagService.getById(id);

        Link selfLink = linkTo(methodOn(TagController.class)
                .getById(id)).withSelfRel();

        Link getAllLink = linkTo(methodOn(TagController.class)
                .get(new HashSet<>(), PageRequest.of(0, 1))).withRel("tags");

        EntityModel<TagDto> result = EntityModel.of(tagDto, selfLink, getAllLink);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Creating new Tag
     *
     * @param tagDto @RequestBody
     * @return ResponseEntity<?>
     */
    @PostMapping("/tag")
    public ResponseEntity<?> create(@RequestBody TagDto tagDto) {
        try {

            return new ResponseEntity<>(tagService.create(tagDto), HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Deleting Tag by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @DeleteMapping("/tag/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        try {
            tagService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Getting tags by their name
     *
     * @param name @RequestParam
     * @return ResponseEntity<?>
     */
    @GetMapping("/tag/get-by-name/{name}")
    public ResponseEntity<?> getByName(@PathVariable String name) {
        return new ResponseEntity<>(tagService.getByName(name), HttpStatus.OK);
    }


    /**
     * Method for saving gift certificates and tag into the database, @Transactional will rollback if something went wrong
     *
     * @param tagDto TagDto
     * @return Boolean
     */
//    @Transactional
//    boolean saveGiftCertificateAndTag(TagDto tagDto) {
//        long tagId = tagService.create(tagDto);
//
//        if (tagId == 0)
//            return false;
//
//        tagDto.getGiftCertificateDtos()
//                .forEach(giftCertificateDto -> {
//                    if (giftCertificateDto != null) {
//                        long giftCertificateId = 0;
//                        if (giftCertificateDto.getId() == null) {
//                            giftCertificateId = giftCertificateService.create(giftCertificateDto);
//                            if (giftCertificateId != 0)
//                                giftCertificateService.saveToGiftCertificateTags(giftCertificateId, tagId);
//                            else
//                                throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " (COULD NOT CREATE GIFT CERTIFICATE ) ");
//                        } else
//                            giftCertificateService.saveToGiftCertificateTags(giftCertificateDto.getId(), tagId);
//                    }
//                });
//        return true;
//    }

}
