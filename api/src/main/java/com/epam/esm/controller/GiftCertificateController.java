package com.epam.esm.controller;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.PagedResult;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.services.GiftCertificateServiceV2;
import com.epam.esm.services.TagServiceV2;
import com.epam.esm.specs.GiftCertificateSpecs;
import com.epam.esm.utils.DateFormatter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * GiftCertificate controller for connecting with client
 *
 * @author Abduvohid Isroilov
 */
@RestController
public class GiftCertificateController {

    private final GiftCertificateServiceV2 giftCertificateService;
    private final TagServiceV2 tagService;

    public GiftCertificateController(GiftCertificateServiceV2 giftCertificateService, TagServiceV2 tagService) {
        this.giftCertificateService = giftCertificateService;
        this.tagService = tagService;
    }

    /**
     * Getting all gift certificates also can sort them using this controller method
     *
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate")
    public ResponseEntity<?> get(
            @RequestParam(value = "description", required = false) Set<String> descriptions,
            @RequestParam(value = "price", required = false) Set<Double> prices,
            @RequestParam(value = "duration", required = false) Set<Integer> durations,
            @RequestParam(value = "name", required = false) Set<String> name,
            @RequestParam(name = "create_date", required = false) Set<String> create_dates,
            @RequestParam(name = "create_date", required = false) Set<String> last_update_dates,
            Pageable pageable
    ) {
        DateFormatter dateFormatter = new DateFormatter();

        GiftCertificateSpecs giftCertificateSpecs = new GiftCertificateSpecs();
        giftCertificateSpecs.setDescriptions(descriptions);
        giftCertificateSpecs.setDurations(durations);
        giftCertificateSpecs.setPrices(prices);
        if (create_dates != null)
            giftCertificateSpecs.setCreated_dates(create_dates.stream().map(dateFormatter::parseTimestamp).collect(Collectors.toSet()));
        if (last_update_dates != null)
            giftCertificateSpecs.setLast_update_dates(last_update_dates.stream().map(dateFormatter::parseTimestamp).collect(Collectors.toSet()));
        giftCertificateSpecs.setNames(name);

        Page<GiftCertificateDto> dtos = giftCertificateService.getAllByPagination(giftCertificateSpecs, pageable);

        setHateoasLinks(dtos);

        PagedResult pagedResult = getPagedResult(dtos);

        return new ResponseEntity<>(pagedResult, HttpStatus.OK);

    }

    private PagedResult getPagedResult(Page<GiftCertificateDto> dtos) {
        PagedResult pagedResult = new PagedResult();
        pagedResult.buildMetadataFrom(dtos);
        pagedResult.setItems(dtos.getContent());
        return pagedResult;
    }

    private void setHateoasLinks(Page<GiftCertificateDto> dtos) {
        dtos.getContent()
                .forEach(dto -> {
                    Link selfLink = linkTo(GiftCertificateController.class).slash("/gift_certificate").withSelfRel();
                    dto.add(selfLink);
                    Link dtoLink = linkTo(methodOn(GiftCertificateController.class)
                            .getById(dto.getId())).withRel("gift_certificate");
                    dto.add(dtoLink);
                    if (dto.getTags().size() > 0) {
                        dto.getTags().forEach(tagDto -> {
                            Link tagLink = linkTo(methodOn(TagController.class).getById(tagDto.getId())).withRel("tag");
                            dto.add(tagLink);
                        });
                    }
                });
    }

    /**
     * Getting gift certificate by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping(value = "/gift_certificate/{id}", produces = {"application/hal+json"})
    public ResponseEntity<?> getById(@PathVariable Long id) {
        Link selfLink = linkTo(methodOn(GiftCertificateController.class)
                .getById(id)).withSelfRel();

        Link getAllLink = linkTo(methodOn(GiftCertificateController.class)
                .get(new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), new HashSet<>(), PageRequest.of(0, 1))).withRel("gift_certificates");

        GiftCertificateDto dto = giftCertificateService.getById(id);

        EntityModel<GiftCertificateDto> result = EntityModel.of(dto, selfLink, getAllLink);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Creating new gift certificate
     *
     * @param giftCertificateDto @RequestBody
     * @return ResponseEntity<?>
     */
    @PostMapping("/gift_certificate")
    public ResponseEntity<?> create(@RequestBody GiftCertificateDto giftCertificateDto) {
        try {
            return new ResponseEntity<>(giftCertificateService.create(giftCertificateDto), HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Deleting gift certificate by its id
     *
     * @param id @PathVariable
     * @return ResponseEntity<?>
     */
    @DeleteMapping("/gift_certificate/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        try {
            giftCertificateService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }

    /**
     * Updating gift certificate only given fields
     *
     * @param giftCertificateDto @RequestBody
     * @return ResponseEntity<?>
     */
    @PutMapping("/gift_certificate")
    public ResponseEntity<?> update(@RequestBody GiftCertificateDto giftCertificateDto) {
        try {
            System.out.println("SADF");
            return new ResponseEntity<>(giftCertificateService.update(giftCertificateDto), HttpStatus.OK);
        } catch (Exception ex) {
            throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( " + ex.getMessage() + " ) ");
        }
    }


    /**
     * Getting gift certificates by tag name
     *
     * @param name @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate/get-by-name/{name}")
    public ResponseEntity<?> getCertificatesByName(@PathVariable String name, @RequestParam("page") int page,
                                                   @RequestParam("size") int size) {
        return new ResponseEntity<>(giftCertificateService.getAllByNamePagination(name, page, size), HttpStatus.OK);
    }

    /**
     * Getting gift certificates by tag name with (and condition)
     *
     * @param tagNames @RequestParam
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate/tags")
    public ResponseEntity<?> getCertificatesByTagName(@RequestParam(value = "tag_name") Set<String> tagNames, Pageable pageable) {

        GiftCertificateSpecs giftCertificateSpecs = new GiftCertificateSpecs();
        giftCertificateSpecs.setTag_names(tagNames);

        Page<GiftCertificateDto> dtos = giftCertificateService.getAllByPagination(giftCertificateSpecs, pageable);

        setHateoasLinks(dtos);

        PagedResult pagedResult = getPagedResult(dtos);

        return new ResponseEntity<>(pagedResult, HttpStatus.OK);
    }

    /**
     * Getting gift certificates by its description
     *
     * @param description @PathVariable
     * @return ResponseEntity<?>
     */
    @GetMapping("/gift_certificate/get-by-description/{description}")
    public ResponseEntity<?> getCertificatesByDescription(@PathVariable String description) {
        return new ResponseEntity<>(giftCertificateService.getAllByDescription(description), HttpStatus.OK);
    }

    /**
     * Updating multiple gift certificates
     *
     * @param giftCertificateDtoList @RequestBody
     * @return ResponseEntity<?>
     */
//    @PutMapping("/gift_certificate/batch-update")
//    public ResponseEntity<?> batchUpdate4giftCertificate(@RequestBody List<GiftCertificateDto> giftCertificateDtoList) {
//        return new ResponseEntity<>(giftCertificateService.batchUpdate(giftCertificateDtoList), HttpStatus.OK);
//    }

    /**
     * Sorting helper method
     *
     * @param sortByName           Boolean
     * @param sortByCreateDate     Boolean
     * @param sortByLastUpdateDate Boolean
     * @return ResponseEntity<?>
     */
//    private ResponseEntity<?> getSorted(Boolean sortByName,
//                                        Boolean sortByCreateDate,
//                                        Boolean sortByLastUpdateDate) {
//        if (sortByName != null) {
//            if (sortByCreateDate != null)
//                return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByCreateDateAndName(sortByCreateDate, sortByName), HttpStatus.OK);
//            if (sortByLastUpdateDate != null)
//                return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByLastUpdateDateAndName(sortByLastUpdateDate, sortByName), HttpStatus.OK);
//            return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByName(sortByName), HttpStatus.OK);
//        }
//
//        if (sortByCreateDate != null)
//            return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByCreateDate(sortByCreateDate), HttpStatus.OK);
//
//        if (sortByLastUpdateDate != null)
//            return new ResponseEntity<>(giftCertificateService.getAllGiftCertificateOrderByLastUpdateDate(sortByLastUpdateDate), HttpStatus.OK);
//        return null;
//    }

    /**
     * Method for saving gift certificate and tags into the database, @Transactional will rollback if something went wrong
     *
     * @param giftCertificateDto GiftCertificateDto
     * @return Boolean
     */
//    @Transactional
//    boolean saveGiftCertificateAndTag(GiftCertificateDto giftCertificateDto) {
//        long giftCertificateId = giftCertificateService.create(giftCertificateDto);
//
//        if (giftCertificateId == 0)
//            throw new RuntimeException("Could not create giftCertificate");
//
//        giftCertificateDto.getTagDtos()
//                .forEach(tagDto -> {
//                    if (tagDto != null) {
//                        long tagId = 0;
//                        if (tagDto.getId() == null) {
//                            tagId = tagService.create(tagDto);
//                            if (tagId != 0)
//                                giftCertificateService.saveToGiftCertificateTags(giftCertificateId, tagId);
//                            else
//                                throw new DatabaseException(ApiErrorMessages.INTERNAL_SERVER_ERROR + " ( COULD NOT CREATE TAG ) ");
//                        } else
//                            giftCertificateService.saveToGiftCertificateTags(giftCertificateId, tagDto.getId());
//                    }
//                });
//        return true;
//    }
}
