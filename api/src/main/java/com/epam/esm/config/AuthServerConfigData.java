package com.epam.esm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "auth.server")
public class AuthServerConfigData {

    private String url;
    private String client;
    private String pass;
    private String grantType;
    private String scope;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getGrantType() {
        return grantType;
    }

    public String getScope() {
        return scope;
    }

    public String getUrl() {
        return url;
    }

    public String getClient() {
        return client;
    }

    public String getPass() {
        return pass;
    }
}
