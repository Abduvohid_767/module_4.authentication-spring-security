package com.epam.esm.value;

import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.AttributeConverter;
import java.util.Set;
import java.util.stream.Collectors;

public enum UserRoles {

    USER(Sets.newHashSet(UserAuthority.USER_READ)),
    ADMIN(Sets.newHashSet(UserAuthority.USER_READ, UserAuthority.USER_WRITE)),
    MANAGER(Sets.newHashSet(UserAuthority.USER_WRITE));

    private final Set<UserAuthority> authorities;

    UserRoles(Set<UserAuthority> authorities) {
        this.authorities = authorities;
    }

    public Set<UserAuthority> getAuthorities() {
        return this.authorities;
    }

    public Set<SimpleGrantedAuthority> getRoleAuthorities() {
        Set<SimpleGrantedAuthority> authorities = getAuthorities().stream()
                .map(userAuthority -> new SimpleGrantedAuthority(userAuthority.getAuthority()))
                .collect(Collectors.toSet());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return authorities;
    }

    public String getShortName() {
        switch (this) {
            case USER:
                return "UR";
            case MANAGER:
                return "UW";
            case ADMIN:
                return "URW";

            default:
                throw new IllegalArgumentException("ROLE [" + this
                        + "] not supported.");
        }
    }

    public static UserRoles fromShortName(String shortName) {

        switch (shortName) {
            case "UR":
                return UserRoles.USER;

            case "UW":
                return UserRoles.MANAGER;

            case "URW":
                return UserRoles.ADMIN;

            default:
                throw new IllegalArgumentException("ShortName [" + shortName
                        + "] not supported.");
        }
    }

    @javax.persistence.Converter(autoApply = true)
    public static class Converter implements AttributeConverter<UserRoles, String> {

        @Override
        public String convertToDatabaseColumn(UserRoles userRoles) {
            return userRoles.getShortName();
        }

        @Override
        public UserRoles convertToEntityAttribute(String s) {
            return UserRoles.fromShortName(s);
        }
    }

}
