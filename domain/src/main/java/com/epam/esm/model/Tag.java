package com.epam.esm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This is the model class for exchanging data with database
 *
 * @author Abduvohid Isroilov
 */
@Entity
@Table(name = "tag")
public class Tag extends BaseEntity {

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags", targetEntity = GiftCertificate.class)
    @JsonIgnore
    private Set<GiftCertificate> giftCertificates = new HashSet<>();

    private String name;

    public Tag(Long id, Set<GiftCertificate> giftCertificates, String name) {
        super(id);
        this.giftCertificates = giftCertificates;
        this.name = name;
    }

    public Tag(Set<GiftCertificate> giftCertificates, String name) {
        this.giftCertificates = giftCertificates;
        this.name = name;
    }

    public Tag() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<GiftCertificate> getGiftCertificates() {
        return giftCertificates;
    }

    public void setGiftCertificates(Set<GiftCertificate> giftCertificates) {
        this.giftCertificates = giftCertificates;
    }
}
