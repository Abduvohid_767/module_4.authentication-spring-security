package com.epam.esm.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "user_orders")
@EntityListeners(AuditingEntityListener.class)
public class UserGiftCertificate extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gift_certificate_id")
    private GiftCertificate giftCertificate;

    @Column(name = "purchase_date")
    private Timestamp purchaseDate;

    @Column(name = "cost")
    private Double cost;

    public UserGiftCertificate(GiftCertificate giftCertificate, Timestamp purchaseDate, Double cost) {
        this.giftCertificate = giftCertificate;
        this.purchaseDate = purchaseDate;
        this.cost = cost;
    }

    public UserGiftCertificate() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GiftCertificate getGiftCertificate() {
        return giftCertificate;
    }

    public void setGiftCertificate(GiftCertificate giftCertificate) {
        this.giftCertificate = giftCertificate;
    }

    public Timestamp getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Timestamp purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserGiftCertificate that = (UserGiftCertificate) o;
        return Objects.equals(user, that.user) &&
                Objects.equals(giftCertificate, that.giftCertificate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, giftCertificate);
    }
}
