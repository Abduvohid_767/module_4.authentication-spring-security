package com.epam.esm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

/**
 * This is the model class for exchanging data with database
 *
 * @author Abduvohid Isroilov
 */
@Entity
@Table(name = "gift_certificate")
public class GiftCertificate extends BaseEntity {

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private Double price;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "create_date")
    private Timestamp create_date;

    @Column(name = "last_update_date")
    private Timestamp last_update_date;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH}, fetch = FetchType.EAGER, targetEntity = Tag.class)
    @JoinTable(name = "gift_certificates_tags",
            joinColumns = @JoinColumn(name = "gift_certificate_id", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id", nullable = false))
    @JsonIgnore
    private Set<Tag> tags = new HashSet<>();

    @OneToMany(cascade = {CascadeType.REFRESH}, mappedBy = "user", orphanRemoval = true)
    private Set<UserGiftCertificate> users = new HashSet<>();

    private String name;

    /**
     * No argument constructor for giftCertificate class
     */
    public GiftCertificate() {
    }


    /**
     * All argument constructor for GiftCertificate
     *
     * @param id
     * @param name
     * @param description
     * @param price
     * @param duration
     * @param create_date
     * @param last_update_date
     * @param tags
     */
    public GiftCertificate(Long id, String description, Double price, Integer duration, Timestamp create_date, Timestamp last_update_date, Set<Tag> tags, Set<UserGiftCertificate> users, String name) {
        super(id);
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.create_date = create_date;
        this.last_update_date = last_update_date;
        this.tags = tags;
        this.users = users;
        this.name = name;
    }

    public GiftCertificate(String description, Double price, Integer duration, Timestamp create_date, Timestamp last_update_date, Set<Tag> tags, Set<UserGiftCertificate> users, String name) {
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.create_date = create_date;
        this.last_update_date = last_update_date;
        this.tags = tags;
        this.users = users;
        this.name = name;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Timestamp create_date) {
        this.create_date = create_date;
    }

    public Date getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(Timestamp last_update_date) {
        this.last_update_date = last_update_date;
    }

    public void addTags(Set<Tag> newTags) {
        if (newTags != null) {
            tags.addAll(newTags);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserGiftCertificate> getUsers() {
        return users;
    }

    public void setUsers(Set<UserGiftCertificate> users) {
        this.users = users;
    }

}
