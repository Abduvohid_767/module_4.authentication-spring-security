package com.epam.esm.dto;

import org.springframework.data.domain.Sort;

public interface ResultItems<T> {

    T getItems();

    void setItems(T items);

    Metadata getMetadata();

    void setMetadata(Metadata metadata);

    interface Metadata {
    }

    class PagedMetadata implements Metadata {
        private int totalPages;
        private long totalItems;
        private int page;
        private int size;
        private Sort sort;
        private boolean hasPrevious;
        private boolean hasNext;

        PagedMetadata(int totalPages, long totalItems, int page, int size, Sort sort, boolean hasPrevious, boolean hasNext) {
            this.totalPages = totalPages;
            this.totalItems = totalItems;
            this.page = page;
            this.size = size;
            this.sort = sort;
            this.hasPrevious = hasPrevious;
            this.hasNext = hasNext;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public long getTotalItems() {
            return totalItems;
        }

        public int getPage() {
            return page;
        }

        public int getSize() {
            return size;
        }

        public boolean isHasNext() {
            return hasNext;
        }

        public boolean isHasPrevious() {
            return hasPrevious;
        }

        public Sort getSort() {
            return sort;
        }
    }
}
