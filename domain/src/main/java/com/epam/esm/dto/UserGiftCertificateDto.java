package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserGiftCertificateDto extends BaseDto {

    private GiftCertificateDto giftCertificate;
    private Timestamp purchaseDate;
    private Double cost;

    public UserGiftCertificateDto() {
    }

    public UserGiftCertificateDto(GiftCertificateDto giftCertificate, Timestamp purchaseDate, Double cost) {
        this.giftCertificate = giftCertificate;
        this.purchaseDate = purchaseDate;
        this.cost = cost;
    }

    public UserGiftCertificateDto(Long id, GiftCertificateDto giftCertificate, Timestamp purchaseDate, Double cost) {
        super(id);
        this.giftCertificate = giftCertificate;
        this.purchaseDate = purchaseDate;
        this.cost = cost;
    }

    public GiftCertificateDto getGiftCertificate() {
        return giftCertificate;
    }

    public void setGiftCertificate(GiftCertificateDto giftCertificate) {
        this.giftCertificate = giftCertificate;
    }

    public Timestamp getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Timestamp purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "UserGiftCertificateDto{" +
                ", giftCertificate=" + giftCertificate +
                ", purchaseDate=" + purchaseDate +
                ", cost=" + cost +
                ", id=" + getId() +
                '}';
    }
}
