package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Page;

import java.util.Collection;

public class PagedResult implements ResultItems<Collection> {
    @JsonProperty
    private Collection items;
    @JsonProperty
    private Metadata metadata;

    @Override
    public Collection getItems() {
        return items;
    }

    @Override
    public void setItems(Collection items) {
        this.items = items;
    }

    @Override
    public Metadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public void buildMetadataFrom(Page page) {
        metadata = new PagedMetadata(
                page.getTotalPages(),
                page.getTotalElements(),
                page.getNumber(),
                page.getSize(),
                page.getSort(),
                page.hasPrevious(),
                page.hasNext());
    }
}
