package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * This is the dto class for exchanging data with client and service
 *
 * @author Abduvohid Isroilov
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TagDto extends BaseDto {

    private String name;

    public TagDto() {
    }

    public TagDto(String name) {
        this.name = name;
    }

    public TagDto(Long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TagDto{" +
                "name='" + name + '\'' +
                ", id=" + getId() +
                '}';
    }
}
