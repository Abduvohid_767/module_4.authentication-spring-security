package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserDto extends BaseDto {

    private String phoneNumber;
    private String firstName;
    private String lastName;
    private String address;
    private String age;
    @JsonProperty("gift_certificates")
    private Set<UserGiftCertificateDto> giftCertificates;

    public UserDto() {
    }

    public UserDto(String phoneNumber, String firstName, String lastName, String address, String age, Set<UserGiftCertificateDto> giftCertificates) {
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.age = age;
        this.giftCertificates = giftCertificates;
    }

    public UserDto(Long id, String phoneNumber, String firstName, String lastName, String address, String age, Set<UserGiftCertificateDto> giftCertificates) {
        super(id);
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.age = age;
        this.giftCertificates = giftCertificates;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Set<UserGiftCertificateDto> getGiftCertificates() {
        return giftCertificates;
    }

    public void setGiftCertificates(Set<UserGiftCertificateDto> giftCertificates) {
        this.giftCertificates = giftCertificates;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", age='" + age + '\'' +
                ", giftCertificates=" + giftCertificates +
                ", id=" + getId() +
                '}';
    }
}
