package com.epam.esm.mapping;

import com.epam.esm.dto.UserGiftCertificateDto;
import com.epam.esm.model.UserGiftCertificate;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
        componentModel = "spring",
        uses = {GiftCertificateMapper.class, UserMapper.class},
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface UserGiftCertificateMapper extends BaseMapper<UserGiftCertificate, UserGiftCertificateDto> {
}
