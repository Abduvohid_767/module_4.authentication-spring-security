package com.epam.esm.mapping;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.utils.DateFormatter;
import org.mapstruct.*;

import java.sql.Timestamp;


@Mapper(
        componentModel = "spring",
        uses = {TagMapper.class},
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED
)
public interface GiftCertificateMapper extends BaseMapper<GiftCertificate, GiftCertificateDto> {

    @Mappings({
            @Mapping(source = "create_date", target = "create_date", qualifiedByName = "stringToTimestamp"),
            @Mapping(source = "last_update_date", target = "last_update_date", qualifiedByName = "stringToTimestamp")
    })
    GiftCertificate giftCertificateUpdateFromGiftCertificateDto(GiftCertificateDto giftCertificateDto, @MappingTarget GiftCertificate giftCertificate);

    @Mappings({
            @Mapping(source = "create_date", target = "create_date", qualifiedByName = "stringToTimestamp"),
            @Mapping(source = "last_update_date", target = "last_update_date", qualifiedByName = "stringToTimestamp")
    })
    GiftCertificate dtoToEntity(GiftCertificateDto dto);

    @Named("stringToTimestamp")
    static Timestamp stringToTimestamp(String date) {
        DateFormatter dateFormatter = new DateFormatter();
        return dateFormatter.parseTimestamp(date);
    }

}
