package com.epam.esm.constant;

public class Queries {

    public static final String getMaxCountTagAndHighestCost =
            "select t2.id id ,t2.name name ,u.cost max_cost from  user_orders u \n" +
                    "left join gift_certificates_tags gct2 on gct2.gift_certificate_id=u.gift_certificate_id \n" +
                    "left join tag t2 on t2.id=gct2.tag_id \n" +
                    "where u.gift_certificate_id in ( \n" +
                    "select gct1.gift_certificate_id from gift_certificates_tags gct1 where gct1.tag_id=( \n" +
                    "select ta.id from tag ta where ta.name=( \n" +
                    "select name from gift_certificates_tags g \n" +
                    "left join tag t on t.id = g.tag_id \n" +
                    "left join user_orders uo on uo.gift_certificate_id=g.gift_certificate_id \n" +
                    "where t.id in (select gct.tag_id from gift_certificates_tags gct where gct.gift_certificate_id in ( \n" +
                    "select uo.gift_certificate_id from user_orders uo where uo.user_id=? \n" +
                    ")) group by name order By COUNT(name) DESC LIMIT 1))) and u.user_id=? ORDER BY u.cost DESC LIMIT 1";


}
