//package com.epam.esm.init;
//
//import com.epam.esm.model.User;
//import com.epam.esm.repo.UserRepository;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import java.util.LinkedList;
//import java.util.List;
//
//@Component
//public class DataInit implements CommandLineRunner {
//
//
//    private final UserRepository userRepository;
//
//    public DataInit(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//        List<User> users = new LinkedList<>();
//        users = userRepository.findAll();
//        for (int i = 0; i < users.size(); i++) {
//            if (users.get(i) != null) {
//                User user = users.get(i);
//                user.setUsername(user.getFirstName());
//                user.setEnable(true);
//                userRepository.save(user);
//            }
//        }
//    }
//
//}
