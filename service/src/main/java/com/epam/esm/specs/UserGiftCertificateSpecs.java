package com.epam.esm.specs;

import com.epam.esm.model.UserGiftCertificate;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserGiftCertificateSpecs extends BaseSpecs<UserGiftCertificate> {

    private Set<Double> costs = new HashSet<>();

    public Set<Double> getCosts() {
        return costs;
    }

    public void setCosts(Set<Double> costs) {
        if (costs != null)
            this.costs.addAll(costs);
    }

    @Override
    public Predicate toPredicateChild(Root<UserGiftCertificate> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        List<Predicate> predicates = new ArrayList<>(1);
        if (!costs.isEmpty()) {
            predicates.add(root.get("cost").in(costs));
        }

        query.distinct(true);

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));

    }

}
