package com.epam.esm.specs;

import com.epam.esm.model.Tag;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class TagSpecs extends BaseSpecs<Tag> {

    private Set<String> names = new HashSet<>();

    public Set<String> getNames() {
        return names;
    }

    public void setNames(Set<String> names) {
        if (names != null)
            this.names.addAll(names);
    }

    @Override
    public Predicate toPredicateChild(Root<Tag> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>(1);
        if (!names.isEmpty()) {
            predicates.add(root.get("name").in(names));
        }

        query.distinct(true);

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }

}
