package com.epam.esm.specs;

import com.epam.esm.model.GiftCertificate;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class GiftCertificateSpecs extends BaseSpecs<GiftCertificate> {

    private Set<String> descriptions = new HashSet<>();
    private Set<Double> prices = new HashSet<>();
    private Set<Integer> durations = new HashSet<>();
    private Set<Timestamp> created_dates = new HashSet<>();
    private Set<Timestamp> last_update_dates = new HashSet<>();
    private Set<String> names = new HashSet<>();
    private Set<String> tag_names = new HashSet<>();


    public Set<Timestamp> getCreated_dates() {
        return created_dates;
    }

    public void setCreated_dates(Set<Timestamp> created_dates) {
        if (created_dates != null)
            this.created_dates.addAll(created_dates);
    }

    public Set<Timestamp> getLast_update_dates() {
        return last_update_dates;
    }

    public void setLast_update_dates(Set<Timestamp> last_update_dates) {
        if (last_update_dates != null)
            this.last_update_dates.addAll(last_update_dates);
    }

    public Set<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Set<String> descriptions) {
        if (descriptions != null)
            this.descriptions.addAll(descriptions);
    }

    public Set<Double> getPrices() {
        return prices;
    }

    public void setPrices(Set<Double> prices) {
        if (prices != null)
            this.prices.addAll(prices);
    }

    public Set<Integer> getDurations() {
        return durations;
    }

    public void setDurations(Set<Integer> durations) {
        if (durations != null)
            this.durations.addAll(durations);
    }


    public Set<String> getNames() {
        return names;
    }

    public void setNames(Set<String> names) {
        if (names != null)
            this.names.addAll(names);
    }

    public Set<String> getTag_names() {
        return tag_names;
    }

    public void setTag_names(Set<String> tag_names) {
        if (tag_names != null)
            this.tag_names.addAll(tag_names);
    }

    @Override
    public Predicate toPredicateChild(Root<GiftCertificate> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        List<Predicate> predicates = new ArrayList<>(7);
        if (!descriptions.isEmpty()) {
            predicates.add(root.get("description").in(descriptions));
        }

        if (!prices.isEmpty()) {
            predicates.add(root.get("price").in(prices));
        }

        if (!durations.isEmpty()) {
            predicates.add(root.get("duration").in(durations));
        }

        if (!created_dates.isEmpty()) {
            predicates.add(root.get("create_date").in(created_dates));
        }

        if (!last_update_dates.isEmpty()) {
            predicates.add(root.get("last_update_date").in(last_update_dates));
        }

        if (!names.isEmpty()) {
            predicates.add(root.get("name").in(names));
        }

        if (!tag_names.isEmpty()) {
            tag_names.forEach(name -> predicates.add(cb.equal(root.join("tags").get("name"), name)));
        }

        query.distinct(true);

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
