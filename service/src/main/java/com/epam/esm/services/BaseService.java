package com.epam.esm.services;

import com.epam.esm.dto.BaseDto;
import com.epam.esm.exception.BadRequestException;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapping.BaseMapper;
import com.epam.esm.model.BaseEntity;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.repo.BaseRepository;
import com.epam.esm.specs.BaseSpecs;
import com.epam.esm.specs.GiftCertificateSpecs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Base Service for the common CRUD operations
 * <R> should extends BaseDao,<M> should extends BaseMapper, <E> should extends BaseEntity, <D> should extends BaseDto
 * All methods will call their dependent repository
 *
 * @param <R>
 * @param <M>
 * @param <E>
 * @param <D>
 * @author Abduvohid Isroilov
 */
public abstract class BaseService<R extends BaseRepository<E>, M extends BaseMapper<E, D>, S extends BaseSpecs<E>,
        E extends BaseEntity, D extends BaseDto> {

    private R repository;
    private M mapper;
    private S specs;

    public S getSpecs() {
        return specs;
    }

    public BaseService(R repository, M mapper, S specs) {
        this.repository = repository;
        this.mapper = mapper;
        this.specs = specs;
    }

    public R getRepository() {
        return repository;
    }

    public M getMapper() {
        return mapper;
    }

    /**
     * Generic get all method
     *
     * @return List<D>
     */
    public List<D> getAll() {
        return repository.findAll().stream()
                .map(entity -> mapper.entityToDto(entity))
                .collect(Collectors.toList());
    }

    /**
     * Generic create(D input) method
     *
     * @param input Dto
     * @return Dto
     */
    @Transactional
    public D create(D input) {
        E entity = mapper.dtoToEntity(input);
        E savedEntity = repository.save(entity);
        return mapper.entityToDto(savedEntity);
    }

    /**
     * Generic getById(long id) method for getting data by id from database
     *
     * @param id long
     * @return <D>
     */
    public D getById(long id) {
        E entity = repository.findById(id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + id + " )"));
        return mapper.entityToDto(entity);
    }


    /**
     * Generic deleteById(long id) method for deleting data from database using id
     *
     * @param id long
     */
    @Transactional
    public void deleteById(long id) {
        repository.findById(id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + id + " )"));
        repository.deleteById(id);
    }

    /**
     * Filter and paginate data
     *
     * @param specs    Specification
     * @param pageable Pageable
     * @return Page<D>
     */
    public Page<D> getAllByPagination(S specs, Pageable pageable) {
        try {
            return repository.findAll(specs, pageable)
                    .map(m -> mapper.entityToDto(m));

        } catch (PropertyReferenceException ex) {
            throw new BadRequestException(ApiErrorMessages.BAD_REQUEST + " ( " + ex.getMessage() + " ) ");
        }

    }

    /**
     * Only filter
     *
     * @param specs Specification
     * @return List<E>
     */
    public List<E> getAllByFiltering(S specs) {
        return repository.findAll(specs);
    }

}
