package com.epam.esm.services;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapping.GiftCertificateMapper;
import com.epam.esm.mapping.TagMapper;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import com.epam.esm.model.UserGiftCertificate;
import com.epam.esm.repo.GiftCertificateRepository;
import com.epam.esm.repo.UserGiftCertificateRepository;
import com.epam.esm.specs.GiftCertificateSpecs;
import com.epam.esm.utils.DateFormatter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GiftCertificateServiceV2 extends BaseService<GiftCertificateRepository, GiftCertificateMapper, GiftCertificateSpecs, GiftCertificate, GiftCertificateDto> {

    private final TagServiceV2 tagServiceV2;
    private final TagMapper tagMapper;
    private final UserGiftCertificateService userGiftCertificateService;
    private final UserGiftCertificateRepository userGiftCertificateRepository;

    public GiftCertificateServiceV2(GiftCertificateRepository repository, GiftCertificateMapper mapper, GiftCertificateSpecs specs, TagServiceV2 tagServiceV2, TagMapper tagMapper, UserGiftCertificateService userGiftCertificateService, UserGiftCertificateRepository userGiftCertificateRepository) {
        super(repository, mapper, specs);
        this.tagServiceV2 = tagServiceV2;
        this.tagMapper = tagMapper;
        this.userGiftCertificateService = userGiftCertificateService;
        this.userGiftCertificateRepository = userGiftCertificateRepository;
    }


    /**
     * Generic deleteByName(String name) method for deleting data from database using name
     *
     * @param name String
     */
    @Transactional
    public void deleteByName(String name) {
        GiftCertificate giftCertificate = getRepository().findByName(name).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH NAME ( " + name + " ) "));
        getRepository().deleteByName(name);
    }

    public GiftCertificateDto update(GiftCertificateDto giftCertificateDto) {
        GiftCertificate giftCertificate = getRepository().findById(giftCertificateDto.getId()).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + giftCertificateDto.getId() + " ) "));

        DateFormatter dateFormatter = new DateFormatter();

        if (giftCertificateDto.getName() != null)
            giftCertificate.setName(giftCertificateDto.getName());

        if (giftCertificateDto.getDescription() != null)
            giftCertificate.setDescription(giftCertificateDto.getName());

        if (giftCertificateDto.getDuration() != null)
            giftCertificate.setDuration(giftCertificateDto.getDuration());

        if (giftCertificateDto.getPrice() != null)
            giftCertificate.setPrice(giftCertificateDto.getPrice());

        if (giftCertificateDto.getLast_update_date() != null)
            giftCertificate.setLast_update_date(dateFormatter.parseTimestamp(giftCertificateDto.getLast_update_date()));

        if (giftCertificateDto.getTags() != null) {
            Set<Tag> tagDtoSet = new HashSet<>();
            giftCertificateDto.getTags().forEach(tagDto -> tagDtoSet.add(tagMapper.dtoToEntity(tagDto)));
            giftCertificate.addTags(tagDtoSet);
        }

        GiftCertificate saved = getRepository().save(giftCertificate);

        return getMapper().entityToDto(saved);
    }

    public List<GiftCertificateDto> getAllByTagName(String tagName) {
        TagDto tagDto = tagServiceV2.getByName(tagName);

        return getAll()
                .stream()
                .filter(f -> f.getTags().contains(tagDto))
                .collect(Collectors.toList());
    }

    public List<GiftCertificateDto> getAllByDescription(String description) {
        return getRepository().findAllByDescription(description)
                .stream()
                .map(giftCertificate -> getMapper().entityToDto(giftCertificate))
                .collect(Collectors.toList());
    }

    public Page<GiftCertificateDto> getAllByNamePagination(String name, int page, int size) {
        return getRepository()
                .getAllByName(name, PageRequest.of(page, size))
                .map(m -> getMapper().entityToDto(m));
    }

    @Override
    public void deleteById(long id) {
        GiftCertificate giftCertificate = getRepository().findById(id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + id + " ) "));
        List<UserGiftCertificate> giftCertificates = userGiftCertificateRepository.getAllByGiftCertificate(giftCertificate);

        for (UserGiftCertificate certificate : giftCertificates) {
            userGiftCertificateService.deleteById(certificate.getId());
        }

        super.deleteById(id);
    }
}
