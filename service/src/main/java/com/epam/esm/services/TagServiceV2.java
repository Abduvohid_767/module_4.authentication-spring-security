package com.epam.esm.services;

import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapping.TagMapper;
import com.epam.esm.model.Tag;
import com.epam.esm.repo.TagRepository;
import com.epam.esm.specs.TagSpecs;
import org.springframework.stereotype.Service;

@Service
public class TagServiceV2 extends BaseService<TagRepository, TagMapper, TagSpecs, Tag, TagDto> {


    public TagServiceV2(TagRepository repository, TagMapper mapper, TagSpecs specs) {
        super(repository, mapper, specs);
    }

    public TagDto getByName(String name) {
        Tag tag = getRepository()
                .findByName(name).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH NAME ( " + name + " )"));
        return getMapper().entityToDto(tag);
    }

}
