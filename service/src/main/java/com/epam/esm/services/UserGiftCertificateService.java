package com.epam.esm.services;

import com.epam.esm.constant.Queries;
import com.epam.esm.dto.UserGiftCertificateDto;
import com.epam.esm.exception.CustomNotFoundException;
import com.epam.esm.exception.handling.ApiErrorMessages;
import com.epam.esm.mapper.TagResultMapper;
import com.epam.esm.mapping.UserGiftCertificateMapper;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.TagResult;
import com.epam.esm.model.User;
import com.epam.esm.model.UserGiftCertificate;
import com.epam.esm.repo.GiftCertificateRepository;
import com.epam.esm.repo.UserGiftCertificateRepository;
import com.epam.esm.repo.UserRepository;
import com.epam.esm.specs.UserGiftCertificateSpecs;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class UserGiftCertificateService extends BaseService<UserGiftCertificateRepository, UserGiftCertificateMapper, UserGiftCertificateSpecs, UserGiftCertificate, UserGiftCertificateDto> {

    private final UserRepository userRepository;
    private final GiftCertificateRepository giftCertificateRepository;
    private final JdbcTemplate jdbcTemplate;


    public UserGiftCertificateService(UserGiftCertificateRepository repository, UserGiftCertificateMapper mapper, UserGiftCertificateSpecs specs, UserRepository userRepository, GiftCertificateRepository giftCertificateRepository,
                                      JdbcTemplate jdbcTemplate) {
        super(repository, mapper, specs);
        this.userRepository = userRepository;
        this.giftCertificateRepository = giftCertificateRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    public UserGiftCertificateDto buyCertificate(Long user_id, Long certificate_id) {
        User user = userRepository.findById(user_id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH USER ID ( " + user_id + " ) "));
        GiftCertificate giftCertificate = giftCertificateRepository.findById(certificate_id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH CERTIFICATE ID ( " + certificate_id + " ) "));

        UserGiftCertificate userGiftCertificate = new UserGiftCertificate();
        userGiftCertificate.setCost(giftCertificate.getPrice());
        userGiftCertificate.setPurchaseDate(new Timestamp(new Date().getTime()));
        userGiftCertificate.setUser(user);
        userGiftCertificate.setGiftCertificate(giftCertificate);

        return getMapper().entityToDto(getRepository().save(userGiftCertificate));
    }

    public List<UserGiftCertificateDto> getAllUserOrders(Long user_id) {

        User user = userRepository.findById(user_id).orElseThrow(() -> new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH ID ( " + user_id + " )"));

        return getMapper().entitiesToDtos(getRepository().getAllByUser(user));

    }

    public TagResult getMaxTagAndHighestCost(Long userId) {
        try {
            return jdbcTemplate.queryForObject(Queries.getMaxCountTagAndHighestCost, new TagResultMapper(),
                    userId, userId);
        } catch (EmptyResultDataAccessException ex) {
            throw new CustomNotFoundException(ApiErrorMessages.NOT_FOUND + " WITH USER ID ( " + userId + " ) ");
        }
    }

}
