package com.epam.esm.repo;

import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.User;
import com.epam.esm.model.UserGiftCertificate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGiftCertificateRepository extends BaseRepository<UserGiftCertificate> {

    List<UserGiftCertificate> getAllByUser(User user);

    List<UserGiftCertificate> getAllByGiftCertificate(GiftCertificate giftCertificate);

}
