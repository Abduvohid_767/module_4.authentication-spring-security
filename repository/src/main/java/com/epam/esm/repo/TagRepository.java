package com.epam.esm.repo;

import com.epam.esm.model.Tag;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TagRepository extends BaseRepository<Tag> {

    Optional<Tag> findByName(String name);

}
