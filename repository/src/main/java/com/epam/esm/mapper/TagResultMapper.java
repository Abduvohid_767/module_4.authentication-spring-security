package com.epam.esm.mapper;

import com.epam.esm.model.TagResult;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TagResultMapper implements RowMapper<TagResult> {

    @Override
    public TagResult mapRow(ResultSet rs, int rowNum) throws SQLException {
        TagResult tagResult = new TagResult();

        tagResult.setId(rs.getLong("id"));
        tagResult.setName(rs.getString("name"));
        tagResult.setMaxCost(rs.getDouble("max_cost"));

        return tagResult;
    }

}
