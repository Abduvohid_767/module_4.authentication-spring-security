package com.epam.esm.database;

import com.epam.esm.model.GiftCertificate;
import org.springframework.stereotype.Component;

/**
 * All used queries for the gift certificate class
 *
 * @author Abduvohid Isroilov
 */
@Component
public class GiftCertificateQueries implements BaseQueries<GiftCertificate> {

    @Override
    public String getAll() {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id";
    }

    @Override
    public String getById() {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id where gc.id = ? ";
    }

    @Override
    public String getByName() {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id where gc.name = ? ";
    }

    @Override
    public String save() {

        return " INSERT INTO gift_certificate ( name, description, price, duration, create_date, last_update_date) VALUES ( ?, ?, ?, ?, ?, ?)";

    }

    @Override
    public String deleteById() {

        return "DELETE FROM gift_certificate WHERE id = ?";
    }

    @Override
    public String updateById(GiftCertificate giftCertificate) {
        StringBuilder builder = new StringBuilder();

        builder.append("UPDATE gift_certificate set ");

        if (giftCertificate.getName() != null)
            builder.append(" name = ? ");
        if (giftCertificate.getDescription() != null)
            builder.append(" ,description = ? ");
        if (giftCertificate.getDuration() != null)
            builder.append(" ,duration = ? ");
        if (giftCertificate.getPrice() != null)
            builder.append(" ,price = ? ");
        if (giftCertificate.getCreate_date() != null)
            builder.append(" ,create_date = ? ");
        if (giftCertificate.getLast_update_date() != null)
            builder.append(" ,last_update_date = ?");

        builder.append(" where id = ?");

        return String.valueOf(builder);
    }

    @Override
    public String batchUpdate() {
        return "UPDATE gift_certificate set name = ?, description = ?, duration = ?, price = ?, create_date = ?, last_update_date = ? where id = ?";
    }

    public String getGiftCertificateByTagName() {
        return "select t.id tag_id, t.name tag_name, gc.id, gc.name, gc.description, gc.duration, gc.price, gc.create_date, gc.last_update_date from tag t " +
                "\nleft join gift_certificate_tags gct on gct.tag_id=t.id " +
                "\nleft join gift_certificate gc on gc.id=gct.gift_certificate_id where t.name = ? ";
    }

    public String getByDescription() {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id where gc.description = ? ";
    }

    public String getAllOrderByName(boolean isASC) {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id ORDER BY gc.name " + (isASC ? "ASC" : "DESC");
    }

    public String getAllOrderByCreateDate(boolean isASC) {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id ORDER BY gc.create_date " + (isASC ? "ASC" : "DESC");
    }

    public String getAllOrderByLastUpdateDate(boolean isASC) {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id ORDER BY gc.last_update_date " + (isASC ? "ASC" : "DESC");
    }

    public String getAllOrderByLastUpdateDateAndName(boolean isASCLastUpdateDate, boolean isASCName) {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id ORDER BY gc.last_update_date " + (isASCLastUpdateDate ? "ASC" : "DESC")
                + ", gc.name " + (isASCName ? "ASC" : "DESC");
    }

    public String getAllOrderByCreateDateAndName(boolean isASCCreateDate, boolean isASCName) {
        return "SELECT t.id tag_id,t.name tag_name, gc.id, gc.name, gc.description, gc.price, gc.duration, gc.create_date, gc.last_update_date from gift_certificate gc" +
                "\nleft join gift_certificate_tags gct on gct.gift_certificate_id=gc.id " +
                "\nleft join tag t on t.id = gct.tag_id ORDER BY gc.create_date " + (isASCCreateDate ? "ASC" : "DESC")
                + ", gc.name " + (isASCName ? "ASC" : "DESC");
    }

    public String insertIntoGiftCertificateTags() {
        return "INSERT INTO gift_certificate_tags (gift_certificate_id, tag_id) VALUES (?, ?)";

    }


}
